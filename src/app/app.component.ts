import { Component, ChangeDetectorRef } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "ngx-datatable";

  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  selected = [];
  editing = {};
  rows = [
    { id: 1, name: "Austin", gender: "Male", company: "Swimlane" },
    { id: 2, name: "Dany", gender: "Male", company: "KFC" },
    { id: 3, name: "Moolly", gender: "Female", company: "Burger King" },
    { id: 4, name: "colly", gender: "Female", company: "Burger King" },
    { id: 5, name: "aolly", gender: "Female", company: "Burger King" },
    { id: 6, name: "folly", gender: "Female", company: "Burger King" },
    { id: 7, name: "Mqolly", gender: "Female", company: "Burger King" },
    { id: 8, name: "Molly", gender: "Female", company: "Burger King" },
    { id: 9, name: "Moflly", gender: "Female", company: "Burger King" },
    { id: 10, name: "Moqally", gender: "Female", company: "Burger King" }
  ];
  columns = [
    { prop: "id" },
    { prop: "name" },
    { prop: "Gender" },
    { prop: "Company" }
  ];

  constructor(private cd: ChangeDetectorRef) {
    setTimeout(() => {
      this.loadingIndicator = false;
    }, 1500);
  }
  onSelect({ selected }) {
    this.editing = {};
    console.log("Select Event", selected, this.selected);

    this.selected.splice(0, this.selected.length);

    this.selected.push(...selected);
    if (selected.length) {
      selected.map((ele, index) => {
        this.editing[`${ele.id}-name`] = true;
        this.editing[`${ele.id}-gender`] = true;
        this.editing[`${ele.id}-company`] = true;
      });
    } else {
      this.editing = {};
    }
    this.cd.detectChanges();
  }
  onActivate(event) {}
  updateValue(event, cell, rowIndex) {
    debugger
    console.log("inline editing rowIndex", rowIndex);
    this.editing[rowIndex + "-" + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
    console.log("UPDATED!", this.rows[rowIndex][cell]);
  }
  saveRecord() {
    console.log([...this.rows]);
    this.editing = {};
    this.selected = [];
  }
}
